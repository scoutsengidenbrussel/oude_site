<?PHP include("inc.inc"); ?> 
 </head>
 <body>
  <h1><a href="index.php?o=">Welkom!</a> - Scouts en Gidsen Brussel - <a href="frans.php">Bienvenue!</a></h1>
  <p><iframe width="250" height="250" src="pic.php?o=lidgeld&a=5" align="right" frameborder="0"></iframe>Le scoutisme � Bruxelles en n&eacute;erlandais, c'est bien possible.</p>
  <p>La F&eacute;d&eacute;ration <b>"Scouts en Gidsen Vlaanderen"</b> a une dizaine de groupes dans la r&eacute;gion de Bruxelles-capitale, de Uccle � Evere et Woluw&eacute;. </p>
  <p>Chaque samedi ou dimanche nous nous rencontons pour faire du scoutisme dans nos locaux ou en plein air. Bruxelles est une ville verte avec ses parcs et bois. Quelques groupes se situent tout pr�s du For�t de Soignes. </p>
  <p>Mais � Bruxelles ..., il y a beaucoup plus que la nature, l'aventure urbaine est illimit&eacute;e : differentes cultures, couleurs, odeurs. Avec le metro tout est tout pres. </p>
  <p><iframe width="250" height="250" src="pic.php?o=lidgeld&a=5" align="right" frameborder="0"></iframe>Chaque enfant est bienvenue, connaissance de n&eacute;erlandais est souhait&eacute;e. Tous nos membres vont � des &eacute;coles n&eacute;erlandophones, ou un de ses parents est n&eacute;erlandophone. Il y a plein de groupes de scouts francophones dans notre capitale qui acceuillent tout le monde qui pr&eacute;f�re parler fran�ais. Nous pr&eacute;ferons de communiquer en n&eacute;erlandais, m�me avec vous. Voil� notre motivation pour cette information limit&eacute;e en fran�ais. </p>
  <p>Mais si vous avez besoin d'un peu plus d'explication vous pouvez contacter notre representant professionel � Bruxelles: Johan Vreys: 02/411.25.35, <a href="mailto:regiobrussel@scoutsengidsenvlaanderen.be">regiobrussel@scoutsengidsenvlaanderen.be</a></p>
  <p>Plus d'information sur nos groupes � Bruxelles : <a href="index.php?o=groepen">Cliquez ici</a><br />Plus d'information sur Scouts en Gidsen Vlaanderen: <a href="http://www.scoutsengidsenvlaanderen.be/fr" target="_BLANK">http://www.scoutsengidsenvlaanderen.be/fr</a><br />Plus d'information sur les autres f&eacute;derations Belges : <a href="http://www.scouts.be/" target="_BLANK">http://www.scouts.be/</a></p>
 </body>
</html>