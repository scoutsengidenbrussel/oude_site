<?PHP include("inc.inc");$o=$_GET['o']; ?> 
 <style>h1,h2,h3,h4,p{text-align:center;}</style>
 </head>
 <body align="center">
  <table align="center" style="width:80%;"><tr><td>
  <h1><a href="?o=">Welkom!</a> - Scouts en Gidsen Brussel - <a href="frans.php">Bienvenue!</a></h1>
	<?php if ($o=="") { ?>
	<p>Scouting is springlevend in onze hoofdstad met ongeveer 1000 Brusselse leden en leiding, meisjes en jongens, tussen 6 en 18 jaar, elk met een eigen verhaal.</p>
  <p>Scouts en Gidsen Vlaanderen heeft een tiental bloeiende Nederlandstalige <a href="?o=groepen">scoutsgroepen</a> verspreid over het gehele Brussels Hoofdstedelijk Gewest, van onder meer in Ukkel over Molenbeek tot in Laken en Evere. </p>
  <p>Iedere zondag of zaterdag doen wij net zoals in Vlaanderen en wereldwijd aan scouting. Regelmatig engagement, week in week uit, jaar in jaar uit, dat is een stuk de ziel van scouting. </p>
  <p>Scouts en gidsen lopen langs straten en pleinen, <a href="?o=spelen">spelen</a> hun eigenste spel, bouwen fraaie constructies, trekken er op uit om de omgeving, eigen mogelijkheden en beperkingen te ontdekken. </p>
  <h3>Wil jij met ons mee Brussel en de wereld ontdekken?</h3>
  <p>Dat kan, <b>iedereen</b> kan erbij � met een kleine of een grote portemonnee, met elke geloofsovertuiging. </p>
  <p>We doen een bijzondere inspanning om scouting voor iedereen mogelijk en plezant te maken, met verminderd <a href="?o=prijs">lidgeld</a> indien nodig en bijvoorbeeld een aangepaste werking voor kinderen met een handicap (<a href="?o=akabe">Akabe</a>).</p>
	<?php } ?>
  <h1><a href="?o=groepen">Groepen</a></h1>
  <?php if ($o=="groepen") { $g=$_GET['g'];?>
	<h3><a href="?g=B12/03G&o=groepen">Evere</a></h3>
  <?php if ($g=="B12/03G") { ?>
	<h4>Groepsnaam</h4>
	<p>Ridders van Onze-Lieve-Vrouw.</p>
	<h4>Zaterdag/Zondag</h4>
	<p>Zondag van 14u tot 17u30.<br />1 maal per maand vanaf 10u en 1 maal in de maand niet.<br />2 weekends en 1 kamp per jaar.</p>
	<h4>Gemengd/niet Gemengd</h4>
	<p>Gemengd. We hebben takken(leeftijdsgroepen per 3 jaar) van 10 tot 20 leden.</p>
	<h4>5 troeven van de scoutsgroep</h4>
	<table align="center"><tr><td><ul>
	 <li>Vlakbij 2 parken en op 20 minuten van hartje Brussel met het openbaar vervoer</li>
	 <li>Kleine groep, waardoor iedereen iedereen kent.</li>
	 <li>Gemotiveerde leidingsploeg</li>
	 <li>Mooie lokalen</li>
	 <li>En zoveel meer...</li>
	</ul></td></tr></table>
	<h4>locatie</h4>
	<p><img src="../wiezitwaar/evere.gif" /></p>
	<h4></h4>
	<p></p>
	<h4>Groepsleiding</h4>
	<p><i>Klaas De Schouwer</i><br /><br /><img src='http://www.scoutsevere.be/images/leiding/KlaasDeSchouwer.jpg' alt='Klaas De Schouwer' height='125' /><br /><br />Verdunstraat 36<br />1130 Haren<br />0474/643.815<br /><a href='mailto:Klaas@scoutsevere.be'>Klaas@scoutsevere.be</a></p>   
  <?PHP } ?>
	<h3><a href="?g=B&o=groepen">Ukkel</a></h3>
  <?php if ($g=="B") { ?>
	<h4>Nog in te vullen</h4>
	<p></p>   
  <?PHP } ?>
	<h3><a href="?g=B&o=groepen"></a></h3>
  <?php if ($g=="B") { ?>
	<h4></h4>
	<p></p>   
  <?PHP } ?>
	<?php } ?>
	<h1><a href="?o=spelen">Spelen/Activiteiten</a></h1>
  <?php if ($o=="spelen") { ?>
  <p>De natuur is ook in Brussel onze troef. Dit is mogelijk dankzij de vele parken en bossen van het groene Brussel. Enkele groepen bevinden zich op wandelafstand van het Zoni�nwoud.</p>
  <p>Wij doen het dus ook in de natuur, maar eerlijk gezegd valt er in de grootstad zelf nog meer avontuur te beleven : zoveel culturen, geuren, kleuren! Op dauwtrip doorheen een ontwakende stad zie je gegarandeerd rare vogels en bijzondere taferelen. Een ritje met de eerste tram naar Tervuren is een echte wildsafari. </p>
  <p>Al spelend kan je veel leren: jezelf uitdrukken, samenwerken, creatief naar oplossingen zoeken, overleggen en kiezen tussen goed en kwaad. Spelen is leren. Spelen maakt mensen behendig, krachtig, sociaal en zelfstandig. Spelen is pret.</p>
	<?php } ?>
	<h1><a href="?o=prijs">Kostprijs</a></h1>
  <?php if ($o=="prijs") { ?>
	op te vullen.
	<?php } ?>
	<h1><a href="?o=akabe">AKABE</a></h1>
	<?php if ($o=="akabe") { ?>
	<h3>Wie zijn wij?</h3>
  <p>De AKABE-tak van ons district, &quot;VVKSM 't Klauwtje&quot; maakt deel uit van de groep VVKSM De Klauwaert. &quot;AKABE&quot; staat voor &quot;Anders KAn BEst&quot; en richt zich tot personen met een handicap.<br /><br /><img src="../akabe/indiaan.jpg" width="135" height="169" border="0" align="left" />Afgelopen maanden werden een vijftal gemotiveerde leid(st)er(s) uit diverse groepen gevonden met een pak scoutservaring, die de uitdaging willen aangaan om zich op een fijne manier en met veel enthousiasme bezig te houden met deze kinderen.</p>
  <hr />
  <h3>Wat doen we?</h3>
  <p>Net zoals alle andere scouts en gidsen willen we ook deze kinderen laten proeven van het samen spelen en avonturen beleven, kortweg van het ware scoutsgevoel!<br /><br />We komen twee zaterdagen per maand samen voor een namiddag- of dagactiviteit en trekken tijdens de zomervakantie ook op kamp.<img src="../akabe/tshirt.jpg" width=135 height=169 align="right"><br /><br />Ravotten, rustig genieten, kampen bouwen, knutselen, activiteiten naar ieders interesse... alle ingredi&euml;nten zijn aanwezig voor een zinvolle en bijzonder leuke scoutse ontspanning! </p>
  <hr />
  <h3>Doelgroep?</h3>
  <p>Onze groep stelt zich graag open voor jonge kinderen met een (licht) mentale handicap (type 1 &amp; type 2). Kinderen tss. 6 en 14 jaar zijn welkom.<br /><br />We komen twee zaterdagen in de maand samen. De eerste activiteiten gingen van start begin 2005<br /><br />Onze lokalen kan je vinden op de Wemmelsesteenweg 310 te Jette. Deze liggen net naast het park van Jette en vlot bereikbaar met het openbaar vervoer (trein - bus - tram).</p>
	<?php } ?>
	<hr />
	<p>Wil je meer weten over de waarden en aanpak van Scouts en Gidsen Vlaanderen: <a href="http://www.scoutsengidsenvlaanderen.be/iedereen/" target="_blank">http://www.scoutsengidsenvlaanderen.be/iedereen/</a><br />
  Andere vragen krijgen een antwoord van onze stadsondersteuner, Johan Vreys: 02/411.25.35, <a href="mailto:regiobrussel@scoutsengidsenvlaanderen.be"> regiobrussel@scoutsengidsenvlaanderen.be</a></p>
  <p><b>Met de steun van de Vlaamse Gemeenschapscommissie</b><br /><img src="vgc.gif" alt="vgc" height='50' align="middle" /></p>
	</td></tr></table>
 </body>
</html>