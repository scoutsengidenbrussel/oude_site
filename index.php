<?PHP include("inc.inc");$o=$_GET['o']; ?> 
 <style>h1,h2,h3,h4,p{text-align:center;}</style>
 </head>
 <body align="center">
  <table align="center" style="width:80%;"><tr><td>
  <h1><a href="?o=">Welkom!</a> - Scouts en Gidsen Brussel - <a href="frans.php">Bienvenue!</a></h1>
	<p><iframe width="250" height="250" src="pic.php?o=welkom&a=5" align="right" frameborder="0"></iframe>Scouting is springlevend in onze hoofdstad met ongeveer 1000 Brusselse leden en leiding, meisjes en jongens, tussen 6 en 18 jaar, elk met een eigen verhaal.</p>
  <p><iframe width="250" height="250" src="pic.php?o=welkom&a=5" align="left" frameborder="0"></iframe>Scouts en Gidsen Vlaanderen heeft een tiental bloeiende Nederlandstalige <a href="?o=groepen">scoutsgroepen</a> verspreid over het gehele Brussels Hoofdstedelijk Gewest, van onder meer in Ukkel over Molenbeek tot in Laken en Evere. </p>
  <p>Iedere zondag of zaterdag doen wij net zoals in Vlaanderen en wereldwijd aan scouting. Regelmatig engagement, week in week uit, jaar in jaar uit, dat is een stuk de ziel van scouting. </p>
  <p>Scouts en gidsen lopen langs straten en pleinen, <a href="?o=spelen">spelen</a> hun eigenste spel, bouwen fraaie constructies, trekken er op uit om de omgeving, eigen mogelijkheden en beperkingen te ontdekken. </p>
  <h3>Wil jij met ons mee Brussel en de wereld ontdekken?</h3>
  <p>Dat kan, <b>iedereen</b> kan erbij � met een kleine of een grote portemonnee, met elke geloofsovertuiging. </p>
  <p>We doen een bijzondere inspanning om scouting voor iedereen mogelijk en plezant te maken, met verminderd <a href="?o=prijs">lidgeld</a> indien nodig en bijvoorbeeld een aangepaste werking voor kinderen met een handicap (<a href="?o=akabe">Akabe</a>).</p>
	<?php if ($o=="spelen") { ?>
	<hr />
	<h1>Spelen/Activiteiten</h1>
  <p><iframe width="250" height="250" src="pic.php?o=spelen&a=9" align="right" frameborder="0"></iframe>De natuur is ook in Brussel onze troef. Dit is mogelijk dankzij de vele parken en bossen van het groene Brussel. Enkele groepen bevinden zich op wandelafstand van het Zoni�nwoud.</p>
  <p><iframe width="250" height="250" src="pic.php?o=spelen&a=9" align="left" frameborder="0"></iframe>Wij doen het dus ook in de natuur, maar eerlijk gezegd valt er in de grootstad zelf nog meer avontuur te beleven : zoveel culturen, geuren, kleuren! Op dauwtrip doorheen een ontwakende stad zie je gegarandeerd rare vogels en bijzondere taferelen. Een ritje met de eerste tram naar Tervuren is een echte wildsafari. </p>
  <p>Al spelend kan je veel leren: jezelf uitdrukken, samenwerken, creatief naar oplossingen zoeken, overleggen en kiezen tussen goed en kwaad. Spelen is leren. Spelen maakt mensen behendig, krachtig, sociaal en zelfstandig. Spelen is pret.</p>
	<p>couting is begeleid spelen. Zo leer je vanaf 6 jaar verantwoordelijkheid nemen voor jezelf �n voor anderen. Eerst in kleine groep vanaf 8 jaar, vanaf 18 jaar voor een grotere groep. Vanaf 12 jaar doen de leden zelf kleine projecten, waarbij ze leren samenwerken en organiseren.  Zo bereiden we onze leden voor om uiteindelijk zelf activiteiten te begeleiden voor kinderen. We bieden onze leiding ook cursussen aan om nog beter met hun leden om te gaan en we stimuleren hen tot het behalen van een (hoofd)animatorattest. Ons succes bewijst de kwaliteit.</p>
	<?php } if ($o=="prijs") { ?>
	<hr />
	<h1>Lidgeld/Kostprijs</h1>
	<p><iframe width="250" height="250" src="pic.php?o=lidgeld&a=5" align="right" frameborder="0"></iframe><iframe width="250" height="250" src="pic.php?o=lidgeld&a=5" align="left" frameborder="0"></iframe>Scoutsgroepen vragen gemiddeld ongeveer 25 euro lidgeld aan het begin van het jaar. Dit geld wordt gebruikt om de verzekering (23.10 euro) en de werkingskosten (maandblad, knutselmateriaal...) te betalen. Voor weekends, kampen en sommige betalende activiteiten wordt een klein bedrag gevraagd, maar grote kosten zoals het kamp zijn alvast aftrekbaar van de belastingen voor kinderen tot 12 jaar.</p>
  <p>Gezinnen met financi�le moeilijkheden kunnen op eenvoudig verzoek gebruik maken van sociale maatregelen, zoals verminderd lidgeld (dit betekent de helft van de prijs). De leiding zal dit discreet in orde brengen en er rekening mee houden.</p>
  <p>Het uniform bestaat minstens uit enkele kledingstukken: een beige hemd (nieuw prijs +/- 30 euro) en een groepsdas (vanaf 5 euro), aangevuld met groene broek of rok. Vele groepen organiseren een systeem voor tweedehands uniformen, zodat je nog goedkoper aan je uniform kan geraken.</p>
  <p>De beperkte kostprijs mag geen drempel zijn, is dit toch niet betaalbaar voor u: Spreek er over met de (groeps)leiding van je kind. Zij zorgen discreet voor oplossingen en ondertussen kan je kind alvast meedoen!</p>
	<?php } if ($o=="akabe") { ?>
	<hr />
	<h1>AKABE</h1>
	<p>Dit verhaal begon drie jaar geleden: een vaste kern van 3 leidsters en een 7-tal leden doen tweemaal per maand op aangepaste wijze aan scouting in de scoutslokalen van Jette (Wemmelsesteenweg 310) en gaan in de zomermaanden op kamp met een gewone scoutsgroep.</p>
  <p><iframe width="250" height="250" src="pic.php?o=akabe&a=5" align="left" frameborder="0"></iframe>We willen dat kinderen met een handicap ook in Brussel aan scouting kunnen doen en we willen bekendmaken dat ook deze kinderen van harte welkom zijn bij onze jeugdbeweging, vandaar werd in samenwerking met al onze groepen van het Brusselse gewest een AKABE-groep opgericht. Zo garanderen we een aangepast aanbod en stimuleren we alle groepen om te werken aan hun toegankelijkheid.  </p>
  <p><iframe width="250" height="250" src="pic.php?o=akabe&a=5" align="right" frameborder="0"></iframe>Jongeren met een handicap, van 6 tot +/- 20 jaar, die in Brussel aan scouting willen doen, zijn welkom om deel te nemen aan de activiteiten na een verkennend gesprek met ouders of begeleiding om de mogelijkheden te bekijken.<br />Op dit moment bereiken we vooral jongeren met een lichte mentale beperking.</p>
  <p>Akabe is scouting. Helemaal scouting en toch een beetje anders. Maar Anders Kan Best! Binnen VVKSM, de scouts- en gidsenbeweging in Vlaanderen staat Akabe voor de werking met kinderen en jongeren met een handicap.</p>
  <p>Een vlottentocht, een avonturenkamp of een ridder op de maan, je komt het allemaal tegen bij Akabe. Akabe wil kinderen en jongeren met een handicap de kans geven om zich te engageren in de jeugdbeweging en hen op die manier laten deelnemen aan leuke activiteiten op maat. Ze ontdekken hun grenzen, leren ermee omgaan en worden uitgedaagd om ze te verleggen. Ze  bepalen zelf mee waar de tent op kamp het best staat, welk pleinspel er eerst gespeeld wordt, welk project er dit jaar uitgewerkt wordt,� Vrienden maken, met vallen en opstaan. Samen dingen doen. Elkaar een handje helpen. </p>
  <p>Bij onze akabewerking is er voldoende leiding om elk kind en elke jongere de nodige aandacht te geven. Kinderen en jongeren met een handicap hebben, naast een aantal beperkingen, ook v��l mogelijkheden. Die mogelijkheden willen we hier tenvolle ontwikkelen. Daarom kiezen we voor een aanpak op maat. Er wordt gezorgd voor een gevarieerd activiteitenaanbod, waar niet-competitieve spelen op de eerste plaats komen. En leuke en originele inkleding staat steeds centraal!</p>
	<?PHP } ?>
  <hr />
	<h1><a href="?o=groepen">Brusselse Groepen</a></h1>
  <?php if ($o=="groepen") {
	 $g=$_GET['g'];
	 db();
	 $groepenlijst=mysql_query("select gemeente, nummer,naam from groep where nummer<>'B12/00D' and nummer<>'B12/08G' order by gemeente;");
	 while ($groep=mysql_fetch_row($groepenlijst)) {
	  echo "<a name=".$groep[1]."><h3><a href='?o=groepen&g=".$groep[1]."#".$groep[1]."'><b>".$groep[0]." </b>(".$groep[2].")</a></h3></a>";
    if ($g==$groep[1]) {
		 $groepinfo=mysql_fetch_row(mysql_query("select naam, geslacht, weekend, troef1, troef2, troef3, troef4, troef5, origine from groep where nummer='".$groep[1]."';"));
		 $troef1=$groepinfo[3];
		 $troef2=$groepinfo[4];
		 $troef3=$groepinfo[5];
		 $troef4=$groepinfo[6];
		 $troef5=$groepinfo[7];
		 if ($troef1!="") {$troef1="<li>".$troef1."</li>";}
		 if ($troef2!="") {$troef2="<li>".$troef2."</li>";}
		 if ($troef3!="") {$troef3="<li>".$troef3."</li>";}
		 if ($troef4!="") {$troef4="<li>".$troef4."</li>";}
		 if ($troef5!="") {$troef5="<li>".$troef5."</li>";}
	   echo "<h4>Groepsnaam</h4><p>".$groepinfo[0]."<br />".$groepinfo[8]."</p><h4>Zaterdag/Zondag</h4><p>".$groepinfo[2]."</p><h4>Gemengd/niet Gemengd</h4><p>".$groepinfo[1]."</p><h4>Troeven van de scoutsgroep</h4><table align='center'><tr><td><ul>".$troef1.$troef2.$troef3.$troef4.$troef5."</ul></td></tr></table><h4>Locatie</h4><p><img src='../wiezitwaar/".str_replace('/','',$groep[1]).".gif' /></p><h4>Groepsleiding</h4><p><i>";
		 $groepsleiding=mysql_fetch_row(mysql_query("select vnaam, fnaam, tel, mail from leiding, takken where lnr=nr and afk='grl' and groep='".$groep[1]."';"));
		 echo $groepsleiding[1]." ".$groepsleiding[0]."</i><br /><br /><img src='../wiwfotos/".$groepsleiding[1]." ".$groepsleiding[0].".jpg' /><br />".$groepsleiding[2]."<br /><a href='mailto:".$groepsleiding[3]."'>".$groepsleiding[3]."</a></p>";
    }
	 }} ?>
	<hr />
	<p>Wil je meer weten over de waarden en aanpak van Scouts en Gidsen Vlaanderen: <a href="http://www.scoutsengidsenvlaanderen.be/iedereen/" target="_blank">http://www.scoutsengidsenvlaanderen.be/iedereen/</a><br />
  Andere vragen krijgen een antwoord van onze stadsondersteuner, Johan Vreys: 02/411.25.35, <a href="mailto:regiobrussel@scoutsengidsenvlaanderen.be"> regiobrussel@scoutsengidsenvlaanderen.be</a></p>
  <p><b>Met de steun van de Vlaamse Gemeenschapscommissie</b><br /><img src="vgc.gif" alt="vgc" height='50' align="middle" /></p>
	</td></tr></table>
 </body>
</html>